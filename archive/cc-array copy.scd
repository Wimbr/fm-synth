
(

~midi_keyb = "small_one" ;


if (~midi_keyb == "big_one"){
	~knob = [20,21,22,23,24,25,26,27] ;
	~bankSelector_cc = 116 ;
	~arpeggio_cc = 117 ;
	~record_cc = 36 ;
	~stop_record_cc = 37 ;
}{
	~knob = [21,22,23,24,25,26,27,28] ;
	~bankSelector_cc = 116 ;
	~arpeggio_cc = 117 ;
	~record_cc = 114 ;
	~stop_record_cc = 115 ;

};



n = NetAddr("127.0.0.1",7771);
MIDIClient.init ;
MIDIIn.connectAll ;

)

(


~p = MIDIBufManager.new(TempoClock, [0, \omni]);


)
(
// these are the busses
~filterOutputBus = Bus.audio(s,1) ; // audio
~ampEnvelopeOutputBus = Bus.audio(s,1);
~dcOutputBus = Bus.audio(s,1) ;
~filterEnvelopeOutputBus = Bus.audio(s,1) ;
~modEnvelopeOutputBus = Bus.audio(s,1) ;
~distOutputBus = Bus.audio(s,1) ;
~mixerOutputBus = Bus.audio(s,1) ;
~osc2OutputBus = Bus.audio(s,1) ;
~fmOutputBus = Bus.audio(s,1) ;
~sawOutputBus = Bus.audio(s,1) ;
~noiseOutputBus = Bus.audio(s,1) ;
~fmOffsetEnvelopeOutputBus = Bus.audio(s,1) ;
~reverbOutputBus = Bus.audio(s,1) ;
~panOutputBus = Bus.audio(s,1) ;
~filterFreqOutputBus =  Bus.audio(s,1) ;


~triggerOutputBus = Bus.control(s,1) ;
~freqOutputBus = Bus.control(s,1) ;
~portamentoOutputBus = Bus.control(s,1) ;
~lfoOutputBus = Bus.control(s,1) ;
~freqLfoOutputBus = Bus.control(s,1) ;
~freqMultiplyOutputBus = Bus.control(s,1) ;
~modulationLfoOutputBus = Bus.control(s,1) ;

)

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  Synth definitions
*
*  this block defines the different synths for the ms synth
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

(



SynthDef(\dist , {
    arg signalOutputBus = 8 ,
		signalInputBus = 9 ,
	    amount = 0 ;
	var x = In.ar(signalInputBus , 1);
	var k = 2 * amount / (1 - amount);
	var dist = (1 + k) * x / (1 + (k * x.abs));
	Out.ar(signalOutputBus ,dist );
}).add ;




SynthDef(\reverb , {
    arg signalOutputBus = 8 ,
		signalInputBus = 9 ,
	mix = 0.33 ,
	room = 0.5 ,
	damp =  0.5 ;
	var reverb = FreeVerb.ar(In.ar(signalInputBus , 1),mix , room , damp );
	Out.ar(signalOutputBus ,reverb );
}).add ;


SynthDef(\mapper , {
	arg inputSignalA_Bus = 0 ,
	inputSignalB_Bus = 0 ,
	mappedSignalA_Bus = 0 ,
	mqppedSignalB_Bus = 0 ;
	a = In.kr(inputSignalA_Bus , 1);
	b = a ;
	Out.kr(mappedSignalA_Bus , a);
	Out.kr(mqppedSignalB_Bus , b);
}).add  ;


SynthDef(\noteTrigger , {
	arg t_trigOn = 0  , t_trigOff = 0 , signalOutputBus = 100 ;
}).add ;


SynthDef(\dc ,
	{arg signalOutputBus = 9 ;
	Out.ar(signalOutputBus , DC.ar(1));
}).add;

SynthDef(\lfo , {
    arg signalOutputBus = 8 ,
		signalInputBus = 9 ,
		lfoRange = 0 ,
		lfoFrequency = 2 ;
	Out.kr(signalOutputBus , In.kr(signalInputBus , 1) + SinOsc.kr(lfoFrequency,0,lfoRange));
}).add ;

SynthDef(\portamento, {
	// line between previous frequency and the current frequency
	// stores the previous frequency
	arg signalOutputBus = 92 , signalInputBus = 91 , duration = 0 ;
	var portamento = VarLag.kr(In.kr(signalInputBus , 1) , duration, 5 , Env.shapeNumber(\lin) ) ; //var line = EnvGen.kr(Lin
	//var line = Line.kr(oldFreq , newFreq , duration ,doneAction:2);
	Out.kr(signalOutputBus , portamento);
}).add ;


SynthDef.new(\pan ,
	{
		arg signalInputBus = 100 , signalOutputBus = 0 ;
		Out.ar(signalOutputBus , (In.ar(signalInputBus,1))!2);
		//Out.ar(signalOutputBus , In.ar(signalInputBus , 1));
	}
).add;


SynthDef.new(\freq ,
	{
		arg freq = 400 ,
		frequencyOutputBus = 8 ,
		frequencyInputBus = 9 ,
		lfoRange = 0 ,
		lfoFrequency = 2 ;
		//Out.kr(frequencyOutputBus , In.kr(frequencyInputBus , 1) + SinOsc.kr(lfoFrequency,0,lfoRange) );
		Out.kr(frequencyOutputBus , freq + SinOsc.kr(lfoFrequency,0,lfoRange) );
	}
).add;

SynthDef.new(\filter ,
	{
		arg signalInputBus = 2 ,
		egIntensity = 0 ,
		frequency = 400 ,
		gain = 2 ,
		envelopeInputBus = 1 ,
		signalOutputBus = 0 ,
		frequencyInputBus = 12,
		lfo_speed = 1;

		var lfo_value = SinOsc.kr(lfo_speed).range(0,1);
		var ff = (lfo_value * In.ar(envelopeInputBus , 1)).range(frequency ,
			LinLin.kr(egIntensity,-1,1,20,2000));


	//	var filterFrequency = LinLin.kr(
		//	In.ar(envelopeInputBus,1) , 0,1,20,frequency);
		//filterFrequency = frequency ;
		Out.ar(signalOutputBus , MoogFF.ar(In.ar(signalInputBus), ff , gain));
		//Out.ar(signalOutputBus , In.ar(signalInputBus , 1));
	}
).add;

SynthDef.new(\saw ,
	{ arg signalOutputBus = 4 , frequencyInputBus = 8 , pitch = 0  , amplitude = 1;
		var freq = In.kr(frequencyInputBus , 1 );
		// maximum frequency for saw pitch
		var fmax  = (freq.cpsmidi+24).midicps-freq;
		var fmin = freq-(freq.cpsmidi-24).midicps;
		var offsetFreq = LinLin.kr(pitch , -1,1,freq - fmin,freq + fmax);
		Out.ar(signalOutputBus , Saw.ar(offsetFreq,amplitude));
	}
).add;

SynthDef.new(\noise ,
	{ arg signalOutputBus = 4 , frequencyInputBus = 8 , amplitude = 0 ;
		var noise = WhiteNoise.ar(amplitude);
		var freq = In.kr(frequencyInputBus , 1 );
		// maximum frequency for saw pitch
		Out.ar(signalOutputBus , noise);
	}
).add;



SynthDef.new(\envelope , {
	arg signalOutputBus  = 2 ,
	signalInputBus = 3 ,
	attackTime = 0.01 , decayTime = 0.3 , sustainLevel = 0.5 , releaseTime = 0.3 ,
	gate = 0 , t_trigOn = 0 , t_trigOff = 0 ,
	doneAction = 0 ;
	g = gate ;

	e = EnvGen.ar(
		Env.adsr(
			attackTime ,
			decayTime ,
			sustainLevel ,
			releaseTime ) , g ,  doneAction:doneAction);
	//Out.ar(outputBus , In.ar(signalInputBus,1) * 1 ) ;
	Out.ar(signalOutputBus ,e*In.ar(signalInputBus,1)) ;
}).add ;

/*
SynthDef.new(\envelope , {
	arg signalOutputBus  = 2 ,
	signalInputBus = 3 ,
	attackTime = 0.01 , decayTime = 0.3 , sustainLevel = 0.5 , releaseTime = 0.3 ,
	gate = 0 , t_trigOn = 0 , t_trigOff = 0 ,
	doneAction = 0 ;
	g =  SetResetFF.ar(
		// set
		Delay1.ar(t_trigOn)
		,
		// reset
		Trig.ar(t_trigOn) +Trig.ar(t_trigOff);
	);

	e = EnvGen.ar(
		Env.adsr(
			attackTime ,
			decayTime ,
			sustainLevel ,
			releaseTime ) , g ,  doneAction:doneAction);
	//Out.ar(outputBus , In.ar(signalInputBus,1) * 1 ) ;
	Out.ar(signalOutputBus ,e*In.ar(signalInputBus,1)) ;
}).add ;
*/
SynthDef.new(\envelopeProperties , {
	arg outputBus = 4 ,
	attackTime = 0.01 ,
	releaseTime  = 2 ,
	decayTime = 0.3 ,
	sustainLevel = 0.33 ;
	Out.kr(outputBus ,[attackTime,decayTime,sustainLevel,releaseTime]  ) ;
}).add ;

SynthDef.new(\osc2  , {
	arg frequencyInputBus , type = 0  , pitch=0 , tune=0 , sync = 0 , signalOutputBus=111  , modulation = 0 ;
	var syncBuffer = Buffer.alloc(s, 1024, 1);
	var syncUpdate = {
		syncBuffer.sine1([1]);

	};
	var sync_freq = In.kr(frequencyInputBus , 1 );
	var freq = sync_freq +
	LinLin.kr(tune , -1 ,1 , (sync_freq.cpsmidi-1).midicps ,  (sync_freq.cpsmidi+1).midicps ) +
	LinLin.kr(pitch , -1 ,1 , (sync_freq.cpsmidi-24).midicps ,  (sync_freq.cpsmidi+24).midicps );

	var oscillators = [
		VarSaw.ar(freq, 0 , modulation) ,
		SinOsc.ar(freq) ,
		Pulse.ar(freq , modulation),
		SyncSaw.ar(sync_freq , freq),
		LPF.ar(Shaper.ar(syncUpdate.eval(),
			SyncSaw.ar(sync_freq , freq),
			0.75 //scale output down because otherwise it goes between -1.05 and 0.5, distorting...
	    ),800),
		Pulse.ar(freq , modulation)
	] ;
	Out.ar( signalOutputBus , Select.ar((type.floor  + (sync.floor * 2)) , oscillators ));
	//Out.ar(signalOutputBus , SinOsc.ar(freq));
}).add ;

SynthDef.new(\fmOscillator ,
{
	arg frequency = 440 ,
		amplitude = 1 ,
		offset = 0  , // is the note offset +-24 octave
		factor = 2  ,
		depth = 1  , // if depth = 0 there is no FM modulation
		frequencyInputBus = 8 ,
		envelopeInputBus = 7 ,
		offsetEnvelopeInputBus = 6 ,
		signalOutputBus = 5 ;

var roundedFrequency  = In.kr(frequencyInputBus , 1)*factor.round/2 ;
		var modFmax = (roundedFrequency.cpsmidi+24).midicps-roundedFrequency;
		var modFmin = roundedFrequency-(roundedFrequency.cpsmidi-24).midicps;
		var modulator = SinOsc.ar(
			LinLin.kr(In.ar(offsetEnvelopeInputBus , 1) * offset , -1,1 , roundedFrequency-modFmin, roundedFrequency+modFmax),
			0,
			mul:depth);

		var sound = SinOsc.ar(In.kr(frequencyInputBus , 1) + (In.ar(envelopeInputBus , 1) * modulator)) ;
		Out.ar(signalOutputBus , sound * amplitude.range(0,1));
}
).add;

SynthDef.new(\mixer ,
	{
		arg signal_A_InputBus = 4 , signal_B_InputBus = 5 , signal_C_InputBus = 6 ,  signalOutputBus = 3 , volume = 0.5 ;
		Out.ar(signalOutputBus ,
			volume * Mix.ar([
				In.ar(signal_A_InputBus,1) ,
				In.ar(signal_B_InputBus,1),
				In.ar(signal_C_InputBus,1)] ,  ));
	}
).add;








)





(



/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  Synth definitions
*
*  this block defines the patching of the synth blocks
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */




/**/
// this is the patching

~pan = Synth.new(\pan ,
		[
		\signalInputBus : ~reverbOutputBus ,
		\signalOutputBus : 0  ,
		]);


/*



synthConstructor.value(\pan ,[
		\signalInputBus : ~delayOutputBus ,
		\signalOutputBus : 0  ,
] , "panUgen" );
*/

~reverb = Synth.new(\reverb ,
		[
		\signalInputBus : ~ampEnvelopeOutputBus ,
		\signalOutputBus : ~reverbOutputBus  ,
		]);



~ampEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~filterOutputBus ,
		\signalOutputBus : ~ampEnvelopeOutputBus  ,
		\doneAction : 0 ,
		\gate : 0 ]);



~filter = Synth.new(\filter ,  [
	\frequencyInputBus : ~freqOutputBus ,
	\envelopeInputBus :  ~filterEnvelopeOutputBus ,
	\signalInputBus : ~distOutputBus ,
	\signalOutputBus : ~filterOutputBus


]);



~filterEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus : ~filterEnvelopeOutputBus  ,
		\doneAction : 0 ,
		\gate : 0 ]);

~dist = Synth.new(\dist ,
		[
		\signalInputBus : ~mixerOutputBus ,
		\signalOutputBus : ~distOutputBus  ,
		\amount : 0 ]);


~mixer = Synth.new(\mixer , [
	\signal_A_InputBus : ~fmOutputBus ,
	\signal_B_InputBus : ~osc2OutputBus ,
	\signal_C_InputBus : ~noiseOutputBus ,
	\signalOutputBus : ~mixerOutputBus
] );




~noise = Synth.new(\noise , [
	\signalOutputBus : ~noiseOutputBus
]);



/*
~saw = Synth.new(\saw ,  [
	\frequencyInputBus : ~lfoOutputBus ,
	\signalOutputBus : nil ,
]);
*/


~osc2 = Synth.new(\osc2, [
	\frequencyInputBus : ~lfoOutputBus ,
	\signalOutputBus : ~osc2OutputBus ,
	\type : 0 ,
	\sync : 0 ,
	\pitch : 0 ,
	\tune : 0
]);


~fmSynth = Synth.new(\fmOscillator ,
	[
	\frequencyInputBus : ~lfoOutputBus ,
	\signalOutputBus : ~fmOutputBus ,
	\envelopeInputBus : ~modEnvelopeOutputBus ,
	\offsetEnvelopeInputBus : ~fmOffsetEnvelopeOutputBus ]);


~fmOffsetEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus :  ~fmOffsetEnvelopeOutputBus ,
		\doneAction : 0 ,
		\gate : 0 ]);



~modEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus :  ~modEnvelopeOutputBus ,
		\doneAction : 0 ,
		\gate : 0 ]);



~freqLfo = Synth.new(\lfo , [
	\signalInputBus : ~portamentoOutputBus ,
	\signalOutputBus : ~lfoOutputBus
	] );




~portamento = Synth.new(\portamento , [
	\signalInputBus : ~freqOutputBus ,
	\signalOutputBus : ~portamentoOutputBus
	] );



//
~baseFreq = Synth.new(\freq , [
	\frequencyOutputBus : ~freqOutputBus ,
]);


~modulationLfo = Synth.new(\lfo , [
	\signalInputBus : ~dcOutputBus ,
	\signalOutputBus : ~modulationLfoOutputBus
	] );


// creates a contstant 1 signal
~dc = Synth.new(\dc , [\signalOutputBus : ~dcOutputBus ]);





)



(

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  MIDI Control
*
*  this block controls the midi messages
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


// variable definitions should come here


var ccMessageConstructor ;  // function to initiate the osc init message
var synthConstructor ;      // creates synths and add it to the synthDictionary together with its name
~synthDictionary = Dictionary.new;

synthConstructor = {
	arg ugen ,  ugen_args , ugen_id;
	~synthDictionary.put(ugen_id , Synth.new(ugen , ugen_args ) );
};






~bankKey = 0 ;
// synth notes params
~notes = Array.newClear(128);

~lastNote = 40 ;
~newNote = 40 ;
// synth notes params


// trigger functions
/*
~trigOn = {
	arg freq ;
	~baseFreq.set(\freq  , freq) ;
	~ampEnvelope.set(\t_trigOn,1);
	~filterEnvelope.set(\t_trigOn,1);
	~modEnvelope.set(\t_trigOn,1);
	~fmOffsetEnvelope.set(\t_trigOn,1);
};

~trigOff = {
	~portamento.set(\t_trigOff,1);
	~ampEnvelope.set(\t_trigOff,1);
	~filterEnvelope.set(\t_trigOff,1);
	~modEnvelope.set(\t_trigOff,1);
	~fmOffsetEnvelope.set(\t_trigOff,1);
};
*/


~trigOn = {
	/*

	triggers the envelopes
	and sends out a OSC note message

	arguments:
	freq / the frequency to be playes
	*/
	arg freq ;
	~baseFreq.set(\freq  , freq) ;
	~ampEnvelope.set(\gate,1);
	~filterEnvelope.set(\gate,1);
	~modEnvelope.set(\gate,1);
	~fmOffsetEnvelope.set(\gate,1);

	n.sendMsg("/noteOn", freq.cpsmidi);



};

~trigOff = {
	arg freq;
	~portamento.set(\gate,0);
	~ampEnvelope.set(\gate,0);
	~filterEnvelope.set(\gate,0);
	~modEnvelope.set(\gate,0);
	~fmOffsetEnvelope.set(\gate,0);

	n.sendMsg("/noteOff", freq.cpsmidi);
};


MIDIdef.noteOn(\simpleOnKey , {
	| vel , nn |
    ~trigOn.value(nn.midicps);
	~nn = nn ;  // crappy arpeggio implementation
} ) ;

MIDIdef.noteOff(\simpleOffKey , {
	| vel , nn |
~trigOff.value(nn.midicps);

} ) ;


~ccData = (0 : (
	~knob[0] :
	(\name: "OSC1"  ,\ugen : ~fmSynth , \propertyName :  \factor , \min :  2 , \max: 16 ),
	~knob[1] :
	(\name: "OSC1"  ,\ugen : ~fmSynth , \propertyName :  \depth , \min :  1 , \max: 1000 ),
	~knob[2] :
	(\name: "OSC1"  ,\ugen : ~fmSynth , \propertyName :  \offset , \min :  -1 , \max: 1 ),
	~knob[3] :
	(\name: "OSC2"  , \ugen : ~osc2 , \propertyName :  \type , \min :  0 , \max:  2 ) ,
	~knob[4] :
	(\name: "OSC2"  , \ugen : ~osc2 , \propertyName :  \pitch , \min :  -2 , \max:  2 ) ,
	~knob[5] :
	(\name: "OSC2"  , \ugen : ~osc2 , \propertyName :  \tune , \min :  -1 , \max:  1 ) ,
	~knob[6] :
	(\name: "OSC2"  , \ugen : ~osc2 , \propertyName :  \sync , \min :  0 , \max:  1 ),
	~knob[7] :
	(\name: "OSC2"  , \ugen : ~osc2 , \propertyName :  \modulation , \min :  0 , \max:  1 )
	),
1 : (
	~knob[0] :
	(\name: "VCF"  , \ugen : ~filter , \propertyName :  \frequency , \min :  20 , \max: 8000 ),
	~knob[1] :
	(\name: "VCF"  ,\ugen : ~filter , \propertyName :  \gain , \min :  4 , \max: 0.1 ),
	~knob[2] :
	(\name: "VCF"  ,\ugen : ~filter , \propertyName :  \egIntensity , \min :  -1 , \max: 1 ),
	~knob[3] :
	(\name: "VCF", \ugen : ~filter , \propertyName :  \lfo_speed  , \min :  0.25 , \max: 8 ),
	~knob[4] :
	(\name: "MIXER",\ugen : ~fmSynth , \propertyName :  \amplitude , \min :  0 , \max: 1 ),
	~knob[5] :
	(\name: "MIXER",\ugen : ~noise , \propertyName :  \amplitude , \min :  0 , \max: 1 ),
	~knob[6] :
	(\name: "MASTER",\ugen : ~mixer , \propertyName :  \volume , \min :  0 , \max: 1 ),
	~knob[7] :
	(\name: "GLIDE",\ugen : ~portamento , \propertyName :  \duration , \min :  0.0 , \max: 2 )
	),
2 : (
	~knob[0] :
	(\name: "AMP-ADSR",\ugen : ~ampEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	~knob[1] :
	(\name: "AMP-ADSR",\ugen : ~ampEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max:  4 ) ,
	~knob[2] :
	(\name: "AMP-ADSR",\ugen : ~ampEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	~knob[3] :
	(\name: "AMP-ADSR",\ugen : ~ampEnvelope , \propertyName :  \releaseTime, \min :  0.01 , \max: 4 ),
	~knob[4] :
	(\name: "VCF-ADSR",\ugen : ~filterEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	~knob[5] :
	(\name: "VCF-ADSR",\ugen : ~filterEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max: 4 ),
	~knob[6] :
	(\name: "VCF-ADSR",\ugen : ~filterEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	~knob[7] :
	(\name: "VCF-ADSR",\ugen : ~filterEnvelope , \propertyName :  \releaseTime , \min :  0.01 , \max: 4 ),
),
3 : (
	~knob[0] :
	(\name: "FM-MOD-ADSR",\ugen : ~modEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	~knob[1] :
	(\name: "FM-MOD-ADSR",\ugen : ~modEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max: 4 ),
	~knob[2] :
	(\name: "FM-MOD-ADSR",\ugen : ~modEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	~knob[3] :
	(\name: "FM-MOD-ADSR",\ugen : ~modEnvelope , \propertyName :  \releaseTime , \min :  0.01 , \max: 4 ),
	~knob[4] :
	(\name: "FM-OFFSET-ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	~knob[5] :
	(\name: "FM-OFFSET-ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max:  4 ) ,
	~knob[6] :
	(\name: "FM-OFFSET-ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	~knob[7] :
	(\name: "FM-OFFSET-ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \releaseTime, \min :  0.01 , \max: 4 )
),
4 : (
	~knob[0] :
	(\name: "dist", \ugen : ~dist , \propertyName :  \amount , \min :  -0.26 , \max: 0.99 ),
	/*~knob[1] :
	(\name: "LFO1", \ugen : ~freqLfo , \propertyName :  \lfoRange , \min :  0 , \max: 100 ),
	~knob[2] :
	(\name: "LFO1",\ugen : ~freqLfo , \propertyName :  \lfoFrequency , \min :  0.25 , \max: 20 ),
	*/~knob[3] :
	(\name: "reverb",\ugen : ~reverb , \propertyName :  \mix , \min :  0 , \max: 1 ),
	~knob[4] :
	(\name: "reverb",\ugen : ~reverb , \propertyName :  \room , \min :  0 , \max: 1 ),
	~knob[5] :
	(\name: "reverb",\ugen : ~reverb , \propertyName :  \damp , \min :  0 , \max: 1 ),

)
);

/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Sending the cc-parameters as OSC messages for external initialization
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~bankKey = 0 ;
~bankOffset = 48;
~ccNum= 0 ;
~ccNumOffset = 20 ;
~modWheelUgens = () ;
~nn = 40 ;
//~modWheelUgens->Dictionary.new;

// this function creates an osc initialisation message



ccMessageConstructor =  {
	// variables
	var sendData ;
	~ccData.do {
		arg jtem , j ;
		var bankKey ;
		bankKey = ~ccData.findKeyForValue(jtem);
		jtem.do{
			arg item , i ;
			var ccNum ;
			ccNum = jtem.findKeyForValue(item);
			sendData = sendData ++
			( "" ++  (bankKey) ++
				"/" ++ (ccNum-~ccNumOffset) ++
				"/" ++ item.matchAt(\name) ++
				"/" ++ item.matchAt(\propertyName) ++ "|").asString;
		};
	};

	// function code here
	sendData.postln ;
	n.sendMsg("/init",sendData.asString);

};


ccMessageConstructor.value();



/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   updating cc messages coming from midi // knobs turning
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~knobButton = {
	| key , func , ccNum , chan |
	var value ;
	var message ;
	//var norm_func = func - ~knob[0] ;

	var rowData  = ~ccData.matchAt(~bankKey).matchAt(func) ;
			//~ccData.matchAt(~bankKey).matchAt(func).postln;
			value = key.linlin(0,127 , rowData.at(\min) , rowData.at(\max));

	try
	{
		rowData.at(\ugen).set(rowData.at(\propertyName) , value);
		(rowData.at(\name) ++ " - " ++ rowData.at(\propertyName)++":" ++ value).postln;}
	{
		// catch block
		// if func block is nil, then set the value of the highest func value in the cc-row
		//
		|error|
		// get the max value
		var maxFunc = ~ccData.matchAt(~bankKey).keys.maxItem;
		var rowData = ~ccData.matchAt(~bankKey).matchAt(maxFunc);
		var value = key.linlin(0,127 , rowData.at(\min) , rowData.at(\max));
		("error").postln;
		try{
			rowData.at(\ugen).set(rowData.at(\propertyName) , value);
			(rowData.at(\name) ++ " - " ++ rowData.at(\propertyName)++":" ++ value).postln;
			}
		{
			// empy catch block
		}
	};
	//		n.sendMsg("/"++rowData.at(\propertyName), value);
	//message =  ;
	//message.postln ;
	//n.sendMsg(("/cc", (~bankKey) , (func - ~knob[0])  , key.linlin(0,127 ,0 , 1) ));

};

/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Fancy modulation wheel initialization
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/


~modWheelDictionaryUpdate =
{
| key , func , ccNum , chan |
~funcKey = func ;
	("* f :"++~funcKey ++ "b : " ++ ~bankKey ).postln;
try{
if ((~modWheelUgens.matchAt(~bankKey)==nil) ,  {
		("bank key does not exist create key in the dictionary").postln;
			~modWheelUgens.add(~bankKey->Dictionary.new);
	// store in this dictionary second key and its associations
		("ccd::"++~ccData.matchAt(~bankKey).matchAt(~funcKey)).postln;
	~modWheelUgens.matchAt(~bankKey).add(~funcKey -> ~ccData.matchAt(~bankKey).matchAt(~funcKey));

		},
	{
		("bank key does  exist , check if function key matches").postln;
	// bank key does exist
	if((~modWheelUgens.matchAt(~bankKey).matchAt(~funcKey)==nil),{
		// function key does not exist, so store and create
		~modWheelUgens.matchAt(~bankKey).add(~funcKey -> ~ccData.matchAt(~bankKey).matchAt(~funcKey));
	},{
		// item exist, so it should be removed
		~modWheelUgens.matchAt(~bankKey).removeAt(~funcKey);
		// check the number of items in the dictionary, if nil remove it entirely
		if((~modWheelUgens.matchAt(~bankKey).size == nil) , {
				~modWheelUgens.removeAt(~bankKey);})

	});
	"item does exist".postln ;

	}
	);

};
	("mwu"+~modWheelUgens).postln ;
};

~modWheelUpdate = {
	| val | // this is a value between 0 and 1
	val.postln ;
	// iterate over dictionary
	//~dataString ="";

	try{
			w = 1;

		~modWheelUgens.do {
		|item , i |
			b = ~modWheelUgens.findKeyForValue(item);
		item.do {
			| jtem , j |
			f  = item.findKeyForValue(jtem);
			v = val.linlin(0,127 , jtem.at(\min) , jtem.at(\max));
			jtem.at(\ugen).set(jtem.at(\propertyName) , v);

				~temp = ( "" ++  b ++  "/" ++ (f-~ccNumOffset) ++ "/" ++ (round(val.linlin(0,127 ,0 , 1)*100)/100) ++ "/");
			//	~temp =  ("d" ++ f ++ "/" ++ b) ;
			//	~temp.postln;
				~dataString= ~dataString ++ ~temp ;

		//	(jtem).postln;

		}

	};

	};

// here we send the information out
	//	("---->"++~dataString).postln ;
		//n.sendMsg("/ccm", ~dataString.asString);
        ~dataString ="";


};

/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Defining the midibuffer - player function
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~p = MIDIBufManager.new(TempoClock, [0, \omni]);

~playSequence = {
	arg midiBuffer ;

// lets play the sequence

var dur , length , trigOff , midi , t_on , t_off ;
// define the trigOn and trigOff functions

d = midiBuffer.current.durs  ;
l = midiBuffer.current.lengths ;
m = midiBuffer.current.freqs ;


dur 	= 	Routine({loop({d.do({ arg dur ; dur.yield  })})});
length 	= 	Routine({loop({l.do({ arg len ; len.yield   })})});
midi 	= 	Routine({loop({m.do({ arg midi ; midi.yield  })})});

~r = Task({
	var delta ;
	while {
		delta = dur.next ;
		delta.notNil ;
	}{
		var deltaTrigOff , midiNote ;
		deltaTrigOff =length.next ;
		midiNote = midi.next ;
		~trigOn.value(midiNote);
		//("trig on :"+delta + " note : " + midiNote).postln ; // equivalent of trig on
		//TempoClock.default.sched(deltaTrigOff , { ("trig off :" + deltaTrigOff).postln;});
			TempoClock.default.sched(deltaTrigOff , ~trigOff );
		delta.yield ;
	} ;
}).play ;


};


/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  CONTROLLER CODE
*
*  this code maps the incoming cc-messages to the right functions
*  needs cleanup
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


t = TempoClock.new;

MIDIdef.cc(\cc , {
	| key , func , ccNum , chan |
	("key"++key++"func"++func++"ccNum"++ccNum).postln ;

	case
	{ func == 1}{ ~modWheelUpdate.value(key) ; } // move the bank one up
	{ func == ~bankSelector_cc }{
		if (key == 127){    // only function on key down
		~bankKey= ~bankKey + 1 ;
			if (~bankKey > ~ccData.keys.maxItem){ ~bankKey = 0 }}
	} // move the bank one up
	{ func == ~arpeggio_cc}{
		if (key == 127){
			// play the arpeggio
			//a  = ~arpeggiator.value([-12 , 0 , 0 ,24]);
			a  = ~arpeggiator.value([-24 , -12 , 0 , 5 , 12 , 24]);
			t.sched(0, a ) ;
		}{
			// stop the arpeggio
			a.stop();
		}
	} // move the bank one down
	{ func == 118}{ ~bankKey =2;} // move the bank one down
	//{ func == 115}{ ~bankKey =3;} // move the bank one down
	{ func == ~record_cc}{
		case
		{key == 127}
		{
					"recording...".postln ;
		try{
			~r.stop;
			};
	~p.initRecord;
	}
		{key == 0 }{
				"recording...stop".postln ;
			~p.stopRecord;
			~playSequence.value(~p);

		}
	}
	{ func == ~stop_record_cc }{
		~r.stop ;
	}
	{ func == 38}{
		~r.play ;
	}
	{ func == 41}{
		t.tempo = value(key.linexp(0 , 127 , 0.25 , 32)) ;
	}
	// move the bank one down
	{ (func >= ~knob[0])&&(func<= ~knob[7]) }{~knobButton.value(key , func , ccNum , chan);} //turn the knobs
	{ (func >= 48)&&(func<56) }{
		("firstrow").postln;
		//if((key==0) , {"het is nul".postln ; },{("het us iets anders:"++key).postln ;  });
		//~modWheelUpdate.value(key , func , ccNum , chan);
		if ((key==0),{~modWheelDictionaryUpdate.value(key , func-28 , ccNum , chan); }); // only when key value = 0
	} //turn the knobs
	{ (func >= 64)&&(func<71) }{("secondrow").postln;} //turn the knobs
	{ (func >= 80)&&(func<87) }{("thirdrow").postln;} //turn the knobs

	/* end cc medssage system here */

}
);



~arpeggiator =
{
	arg offsets = [0 , 5
	] ;
	Routine(
		{
			loop {
			offsets.do {
				arg item ;
					//("called trigon" ++ (~nn + item).midicps).postln;
					~trigOn.value((~nn + item).midicps);
					//("step " ++ item ).postln ;
						0.5.wait;
					//("called trigoff" ++ (~nn + item).midicps).postln;
					~trigOff.value((~nn + item).midicps);
				1.yield ;
			};
			};
	});
};

a  = ~arpeggiator.value;


)

// shitty code starts here ::

(
t.tempo = 6
)

