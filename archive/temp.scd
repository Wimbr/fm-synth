/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  Synth definitions
*
*  this block defines the different synths for the ms synth
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

(


SynthDef(\comb , {
    arg signalOutputBus = 8 ,
		signalInputBus = 9 ,
	maxdelaytime = 0.2 ,
	delaytime = 0 ,
	decaytime = 6;

	f = CombN.ar(In.ar(signalInputBus , 1),
		maxdelaytime,
		delaytime,
		decaytime
	);
	Out.ar(signalOutputBus ,f );
}).add ;


SynthDef(\mapper , {
	arg inputSignalA_Bus = 0 ,
	inputSignalB_Bus = 0 ,
	mappedSignalA_Bus = 0 ,
	mqppedSignalB_Bus = 0 ;
	a = In.kr(inputSignalA_Bus , 1);
	b = a ;
	Out.kr(mappedSignalA_Bus , a);
	Out.kr(mqppedSignalB_Bus , b);
}).add  ;


SynthDef(\noteTrigger , {
	arg t_trigOn = 0  , t_trigOff = 0 , signalOutputBus = 100 ;
}).add ;


SynthDef(\dc ,
	{arg signalOutputBus = 9 ;
	Out.ar(signalOutputBus , DC.ar(1));
}).add;

SynthDef(\lfo , {
    arg signalOutputBus = 8 ,
		signalInputBus = 9 ,
		lfoRange = 0 ,
		lfoFrequency = 2 ;
	Out.kr(signalOutputBus , In.kr(signalInputBus , 1) + SinOsc.kr(lfoFrequency,0,lfoRange));
}).add ;

SynthDef(\portamento, {
	// line between previous frequency and the current frequency
	// stores the previous frequency
	arg signalOutputBus = 92 , signalInputBus = 91 , duration = 0 ;
	var portamento = VarLag.kr(In.kr(signalInputBus , 1) , duration, 5 , Env.shapeNumber(\lin) ) ; //var line = EnvGen.kr(Lin
	//var line = Line.kr(oldFreq , newFreq , duration ,doneAction:2);
	Out.kr(signalOutputBus , portamento);
}).add ;


SynthDef.new(\pan ,
	{
		arg signalInputBus = 100 , signalOutputBus = 0 ;
		Out.ar(signalOutputBus , (In.ar(signalInputBus,1))!2);
		//Out.ar(signalOutputBus , In.ar(signalInputBus , 1));
	}
).add;


SynthDef.new(\freq ,
	{
		arg freq = 400 ,
		frequencyOutputBus = 8 ,
		frequencyInputBus = 9 ,
		lfoRange = 0 ,
		lfoFrequency = 2 ;
		//Out.kr(frequencyOutputBus , In.kr(frequencyInputBus , 1) + SinOsc.kr(lfoFrequency,0,lfoRange) );
		Out.kr(frequencyOutputBus , freq + SinOsc.kr(lfoFrequency,0,lfoRange) );
	}
).add;

SynthDef.new(\filter ,
	{
		arg signalInputBus = 2 ,
		egIntensity = 0 ,
		frequency = 400 ,
		gain = 2 ,
		envelopeInputBus = 1 ,
		signalOutputBus = 0 ,
		frequencyInputBus = 12  ;

		var ff = In.ar(envelopeInputBus , 1).range(frequency ,
			LinLin.kr(egIntensity,-1,1,20,2000));


	//	var filterFrequency = LinLin.kr(
		//	In.ar(envelopeInputBus,1) , 0,1,20,frequency);
		//filterFrequency = frequency ;
		Out.ar(signalOutputBus , MoogFF.ar(In.ar(signalInputBus), ff , gain));
		//Out.ar(signalOutputBus , In.ar(signalInputBus , 1));
	}
).add;

SynthDef.new(\saw ,
	{ arg signalOutputBus = 4 , frequencyInputBus = 8 , pitch = 0  , amplitude = 1;
		var freq = In.kr(frequencyInputBus , 1 );
		// maximum frequency for saw pitch
		var fmax  = (freq.cpsmidi+24).midicps-freq;
		var fmin = freq-(freq.cpsmidi-24).midicps;
		var offsetFreq = LinLin.kr(pitch , -1,1,freq - fmin,freq + fmax);
		Out.ar(signalOutputBus , Saw.ar(offsetFreq,amplitude));
	}
).add;

SynthDef.new(\noise ,
	{ arg signalOutputBus = 4 , frequencyInputBus = 8 , amplitude = 0 ;
		var noise = WhiteNoise.ar(amplitude);
		var freq = In.kr(frequencyInputBus , 1 );
		// maximum frequency for saw pitch
		Out.ar(signalOutputBus , noise);
	}
).add;



SynthDef.new(\envelope , {
	arg signalOutputBus  = 2 ,
	signalInputBus = 3 ,
	attackTime = 0.01 , decayTime = 0.3 , sustainLevel = 0.5 , releaseTime = 0.3 ,
	gate = 0 , t_trigOn = 0 , t_trigOff = 0 ,
	doneAction = 0 ;
	g = gate ;

	e = EnvGen.ar(
		Env.adsr(
			attackTime ,
			decayTime ,
			sustainLevel ,
			releaseTime ) , g ,  doneAction:doneAction);
	//Out.ar(outputBus , In.ar(signalInputBus,1) * 1 ) ;
	Out.ar(signalOutputBus ,e*In.ar(signalInputBus,1)) ;
}).add ;

/*
SynthDef.new(\envelope , {
	arg signalOutputBus  = 2 ,
	signalInputBus = 3 ,
	attackTime = 0.01 , decayTime = 0.3 , sustainLevel = 0.5 , releaseTime = 0.3 ,
	gate = 0 , t_trigOn = 0 , t_trigOff = 0 ,
	doneAction = 0 ;
	g =  SetResetFF.ar(
		// set
		Delay1.ar(t_trigOn)
		,
		// reset
		Trig.ar(t_trigOn) +Trig.ar(t_trigOff);
	);

	e = EnvGen.ar(
		Env.adsr(
			attackTime ,
			decayTime ,
			sustainLevel ,
			releaseTime ) , g ,  doneAction:doneAction);
	//Out.ar(outputBus , In.ar(signalInputBus,1) * 1 ) ;
	Out.ar(signalOutputBus ,e*In.ar(signalInputBus,1)) ;
}).add ;
*/
SynthDef.new(\envelopeProperties , {
	arg outputBus = 4 ,
	attackTime = 0.01 ,
	releaseTime  = 2 ,
	decayTime = 0.3 ,
	sustainLevel = 0.33 ;
	Out.kr(outputBus ,[attackTime,decayTime,sustainLevel,releaseTime]  ) ;
}).add ;



SynthDef.new(\osc2  , {
	arg frequencyInputBus , type = 0  , pitch , tune , sync = 0 , outputBus ;
	var syncBuffer = Buffer.alloc(s, 1024, 1);
	var syncUpdate = {
		syncBuffer.sine1
	}
	var sync_freq = In.kr(frequencyInputBus , 1 );
	var freq = sync_freq +
	LinLin.kr(tune , -1 ,1 , (sync_freq.cpsmidi-1).midicps ,  (sync_freq.cpsmidi+1).midicps ) +
	LinLin.kr(pitch , -1 ,1 , (sync_freq.cpsmidi-24).midicps ,  (sync_freq.cpsmidi+24).midicps );

	var oscillators = [
		Saw.ar(freq) ,
		SinOsc.ar(freq) ,
		SyncSaw.ar(sync_freq , freq),
		LPF.ar(Shaper.ar(syncBuffer,
			SyncSaw.ar(sync_freq , freq),
			0.75 //scale output down because otherwise it goes between -1.05 and 0.5, distorting...
	    ),800)
	] ;
	Out.ar(outputBus , 0*Select.ar((type  + (sync * 2)) , oscillators ));
}).add ;

SynthDef.new(\fmOscillator ,
{
	arg frequency = 440 ,
		amplitude = 1 ,
		offset = 0  ,
		factor = 2  ,
		depth = 1  ,
		frequencyInputBus = 8 ,
		envelopeInputBus = 7 ,
		offsetEnvelopeInputBus = 6 ,
		signalOutputBus = 5 ;

var roundedFrequency  = In.kr(frequencyInputBus , 1)*factor.round/2 ;
		var modFmax = (roundedFrequency.cpsmidi+24).midicps-roundedFrequency;
		var modFmin = roundedFrequency-(roundedFrequency.cpsmidi-24).midicps;
		var modulator = SinOsc.ar(
			LinLin.kr(In.ar(offsetEnvelopeInputBus , 1) * offset , -1,1 , roundedFrequency-modFmin, roundedFrequency+modFmax),
			0,
			mul:depth);

		var sound = SinOsc.ar(In.kr(frequencyInputBus , 1) + (In.ar(envelopeInputBus , 1) * modulator)) ;
		Out.ar(signalOutputBus , sound * amplitude.range(0,1));
}
).add;

SynthDef.new(\mixer ,
	{
		arg signal_A_InputBus = 4 , signal_B_InputBus = 5 , signal_C_InputBus = 6 ,  signalOutputBus = 3 , volume = 0.5 ;
		Out.ar(signalOutputBus ,
			volume * Mix.ar([
				In.ar(signal_A_InputBus,1) ,
				In.ar(signal_B_InputBus,1),
				In.ar(signal_C_InputBus,1)] ,  ));
	}
).add;


// things that should be initialized once

n = NetAddr("192.168.0.103", 57121);
MIDIClient.init ;
MIDIIn.connectAll ;


// initiate the midi recording buffer
~p = MIDIBufManager.new(TempoClock, [2, \omni]);


// these are the busses
~filterOutputBus = Bus.audio(s,1) ; // audio
~ampEnvelopeOutputBus = Bus.audio(s,1);
~dcOutputBus = Bus.audio(s,1) ;
~filterEnvelopeOutputBus = Bus.audio(s,1) ;
~modEnvelopeOutputBus = Bus.audio(s,1) ;
~mixerOutputBus = Bus.audio(s,1) ;
~osc2OutputBus = Bus.audio(s,1) ;
~noiseOutputBus = Bus.audio(s,1) ;
~fmOffsetEnvelopeOutputBus = Bus.audio(s,1) ;
~delayOutputBus = Bus.audio(s,1) ;
~panOutputBus = Bus.audio(s,1) ;
~filterFreqOutputBus =  Bus.audio(s,1) ;


~fmOutputBus = Bus.control(s,1) ;
~triggerOutputBus = Bus.control(s,1) ;
~freqOutputBus = Bus.control(s,1) ;
~portamentoOutputBus = Bus.control(s,1) ;
~lfoOutputBus = Bus.control(s,1) ;
~freqLfoOutputBus = Bus.control(s,1) ;
~freqMultiplyOutputBus = Bus.control(s,1) ;
~modulationLfoOutputBus = Bus.control(s,1) ;


)





(

// variable definitions should come here

var ccMessageConstructor ;  // function to initiate the osc init message
var synthConstructor ;      // creates synths and add it to the synthDictionary together with its name
~synthDictionary = Dictionary.new;

synthConstructor = {
	arg ugen ,  ugen_args , ugen_id;
	~synthDictionary.put(ugen_id , Synth.new(ugen , ugen_args ) );
};

/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  Synth definitions
*
*  this block defines the patching of the synth blocks
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */





// this is the patching

~pan = Synth.new(\pan ,
		[
		\signalInputBus : ~delayOutputBus ,
		\signalOutputBus : 0  ,
		]);




/*
synthConstructor.value(\pan ,[
		\signalInputBus : ~delayOutputBus ,
		\signalOutputBus : 0  ,
] , "panUgen" );
*/

~comb = Synth.new(\comb ,
		[
		\signalInputBus : ~ampEnvelopeOutputBus ,
		\signalOutputBus : ~delayOutputBus  ,
		]);



~ampEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~filterOutputBus ,
		\signalOutputBus : ~ampEnvelopeOutputBus  ,
		\doneAction : 0 ,
		\gate : 0 ]);



~filter = Synth.new(\filter ,  [
	\frequencyInputBus : ~freqOutputBus ,
	\envelopeInputBus :  ~filterEnvelopeOutputBus ,
	\signalInputBus : ~mixerOutputBus ,
	\signalOutputBus : ~filterOutputBus


]);



~filterEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus : ~filterEnvelopeOutputBus  ,
		\doneAction : 0 ,
		\gate : 0 ]);


~mixer = Synth.new(\mixer , [
	\signal_A_InputBus : ~fmOutputBus ,
	\signal_B_InputBus : ~osc2OutputBus ,
	\signal_C_InputBus : ~noiseOutputBus ,
	\signalOutputBus : ~mixerOutputBus
] );

~noise = Synth.new(\noise , [
	\signalOutputBus : ~noiseOutputBus
]);


~saw = Synth.new(\saw ,  [
	\frequencyInputBus : ~lfoOutputBus ,
	\signalOutputBus : ~osc2OutputBus ,
]);



/*
~osc2 = Synth.new(\osc2, [
	\frequencyInputBus : ~freqOutputBus ,
	\signalOutputBus : ~osc2OutputBus ,
	\type : 0 ,
	\sync : 0 ,
	\pitch : 0 ,
	\tune : 0 ,
	\syncBuffer : b
]);
*/
~fmSynth = Synth.new(\fmOscillator , [
	\frequencyInputBus : ~lfoOutputBus ,
	\signalOutputBus : ~fmOutputBus ,
	\envelopeInputBus : ~modEnvelopeOutputBus ,
	\offsetEnvelopeInputBus : ~fmOffsetEnvelopeOutputBus ,

]);


~fmOffsetEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus :  ~fmOffsetEnvelopeOutputBus ,
		\doneAction : 0 ,
		\gate : 0 ]);



~modEnvelope = Synth.new(\envelope ,
		[
		\signalInputBus : ~dcOutputBus ,
		\signalOutputBus :  ~modEnvelopeOutputBus ,
		\doneAction : 0 ,
		\gate : 0 ]);



~freqLfo = Synth.new(\lfo , [
	\signalInputBus : ~portamentoOutputBus ,
	\signalOutputBus : ~lfoOutputBus
	] );




~portamento = Synth.new(\portamento , [
	\signalInputBus : ~freqOutputBus ,
	\signalOutputBus : ~portamentoOutputBus
	] );



//
~baseFreq = Synth.new(\freq , [
	\frequencyOutputBus : ~freqOutputBus ,
]);


~modulationLfo = Synth.new(\lfo , [
	\signalInputBus : ~dcOutputBus ,
	\signalOutputBus : ~modulationLfoOutputBus
	] );


// creates a contstant 1 signal
~dc = Synth.new(\dc , [\signalOutputBus : ~dcOutputBus ]);







/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  MIDI Control
*
*  this block controls the midi messages
*
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


~bankKey = 0 ;
// synth notes params
~notes = Array.newClear(128);

~lastNote = 40 ;
~newNote = 40 ;
// synth notes params


// trigger functions
/*
~trigOn = {
	arg freq ;
	~baseFreq.set(\freq  , freq) ;
	~ampEnvelope.set(\t_trigOn,1);
	~filterEnvelope.set(\t_trigOn,1);
	~modEnvelope.set(\t_trigOn,1);
	~fmOffsetEnvelope.set(\t_trigOn,1);
};

~trigOff = {
	~portamento.set(\t_trigOff,1);
	~ampEnvelope.set(\t_trigOff,1);
	~filterEnvelope.set(\t_trigOff,1);
	~modEnvelope.set(\t_trigOff,1);
	~fmOffsetEnvelope.set(\t_trigOff,1);
};
*/


~trigOn = {
	arg freq ;
	~baseFreq.set(\freq  , freq) ;
	~ampEnvelope.set(\gate,1);
	~filterEnvelope.set(\gate,1);
	~modEnvelope.set(\gate,1);
	~fmOffsetEnvelope.set(\gate,1);
};

~trigOff = {
	~portamento.set(\gate,0);
	~ampEnvelope.set(\gate,0);
	~filterEnvelope.set(\gate,0);
	~modEnvelope.set(\gate,0);
	~fmOffsetEnvelope.set(\gate,0);
};


MIDIdef.noteOn(\simpleOnKey , {
	| vel , nn |
    ~trigOn.value(nn.midicps);
} ) ;

MIDIdef.noteOff(\simpleOffKey , {
	| vel , nn |
~trigOff.value ;

} ) ;



~ccData = (0 : (
	20 :
	(\name: "OSC1 (FM)"  ,\ugen : ~fmSynth , \propertyName :  \factor , \min :  2 , \max: 16 ),
	21 :
	(\name: "OSC1 (FM)"  ,\ugen : ~fmSynth , \propertyName :  \depth , \min :  1 , \max: 1000 ),
	22 :
	(\name: "OSC1 (FM)"  ,\ugen : ~fmSynth , \propertyName :  \offset , \min :  -1 , \max: 1 ),
	23 :
	(\name: "OSC 2"  , \ugen : ~osc2 , \propertyName :  \type , \min :  0 , \max:  1 ) ,
	24 :
	(\name: "OSC 2"  , \ugen : ~osc2 , \propertyName :  \pitch , \min :  -1 , \max:  1 ) ,
	25 :
	(\name: "OSC 2"  , \ugen : ~osc2 , \propertyName :  \tune , \min :  -1 , \max:  1 ) ,
	25 :
	(\name: "OSC 2"  , \ugen : ~osc2 , \propertyName :  \sync , \min :  0 , \max:  1 )
	),
1 : (
	20 :
	(\name: "VCF"  , \ugen : ~filter , \propertyName :  \frequency , \min :  20 , \max: 8000 ),
	21 :
	(\name: "VCF"  ,\ugen : ~filter , \propertyName :  \gain , \min :  4 , \max: 0.1 ),
	22 :
	(\name: "VCF"  ,\ugen : ~filter , \propertyName :  \egIntensity , \min :  -1 , \max: 1 ),
	23 :
	(\name: "MIXER", \ugen : ~saw , \propertyName :  \amplitude , \min :  0 , \max: 1 ),
	24 :
	(\name: "MIXER",\ugen : ~fmSynth , \propertyName :  \amplitude , \min :  0 , \max: 1 ),
	25 :
	(\name: "MIXER",\ugen : ~noise , \propertyName :  \amplitude , \min :  0 , \max: 1 ),
	26 :
	(\name: "MASTER",\ugen : ~mixer , \propertyName :  \volume , \min :  0 , \max: 1 ),
	27 :
	(\name: "GLIDE",\ugen : ~portamento , \propertyName :  \duration , \min :  0.0 , \max: 2 )
	),
2 : (
	20 :
	(\name: "AMP ADSR",\ugen : ~ampEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	21 :
	(\name: "AMP ADSR",\ugen : ~ampEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max:  4 ) ,
	22 :
	(\name: "AMP ADSR",\ugen : ~ampEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	23 :
	(\name: "AMP ADSR",\ugen : ~ampEnvelope , \propertyName :  \releaseTime, \min :  0.01 , \max: 4 ),
	24 :
	(\name: "VCF ADSR",\ugen : ~filterEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	25 :
	(\name: "VCF ADSR",\ugen : ~filterEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max: 4 ),
	26 :
	(\name: "VCF ADSR",\ugen : ~filterEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	27 :
	(\name: "VCF ADSR",\ugen : ~filterEnvelope , \propertyName :  \releaseTime , \min :  0.01 , \max: 4 ),
),
3 : (
	20 :
	(\name: "FM MOD ADSR",\ugen : ~modEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	21 :
	(\name: "FM MOD ADSR",\ugen : ~modEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max: 4 ),
	22 :
	(\name: "FM MOD ADSR",\ugen : ~modEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	23 :
	(\name: "FM MOD ADSR",\ugen : ~modEnvelope , \propertyName :  \releaseTime , \min :  0.01 , \max: 4 ),
	24 :
	(\name: "LFO", \ugen : ~freqLfo , \propertyName :  \lfoRange , \min :  0 , \max: 100 ),
	25 :
	(\name: "LFO",\ugen : ~freqLfo , \propertyName :  \lfoFrequency , \min :  0.25 , \max: 40 )
),
4 : (
	24 :
	(\name: "FM OFFSET ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \attackTime , \min :  0.01 , \max: 4 ),
	25 :
	(\name: "FM OFFSET ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \decayTime , \min :  0.01 , \max:  4 ) ,
	26 :
	(\name: "FM OFFSET ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \sustainLevel , \min :  0.1 , \max: 1 ),
	27 :
	(\name: "FM OFFSET ADSR",\ugen : ~fmOffsetEnvelope , \propertyName :  \releaseTime, \min :  0.01 , \max: 4 )

)
);

/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Sending the cc-parameters as OSC messages for external initialization
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~bankKey = 0 ;
~bankOffset = 48;
~ccNum= 0 ;
~ccNumOffset = 20 ;
~modWheelUgens = () ;
//~modWheelUgens->Dictionary.new;

// this function creates an osc initialisation message



ccMessageConstructor =  {
	// variables
	var sendData ;
	~ccData.do {
		arg jtem , j ;
		var bankKey ;
		bankKey = ~ccData.findKeyForValue(jtem);
		jtem.do{
			arg item , i ;
			var ccNum ;
			ccNum = jtem.findKeyForValue(item);
			sendData = sendData ++
			( "" ++  (bankKey) ++
				"/" ++ (ccNum-~ccNumOffset) ++
				"/" ++ item.matchAt(\name) ++
				"/" ++ item.matchAt(\propertyName) ++ "|").asString;
		};
	};

	// function code here
	n.sendMsg("/init",sendData.asString);

};


ccMessageConstructor.value();



/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Fancy modulation wheel initialization
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~knobButton = {
	| key , func , ccNum , chan |
	o = ~ccData.matchAt(~bankKey).matchAt(func) ;
			//~ccData.matchAt(~bankKey).matchAt(func).postln;
			v = key.linlin(0,127 , o.at(\min) , o.at(\max));

			try{
				o.at(\ugen).set(o.at(\propertyName) , v);
				(o.at(\propertyName)++":" ++ v).postln;};
			//n.sendMsg("/"++o.at(\propertyName), v);
			n.sendMsg("/cc", (~bankKey) , (func-20)  , key.linlin(0,127 ,0 , 1) );

};

~modWheelDictionaryUpdate =
{
| key , func , ccNum , chan |
~funcKey = func ;
	("* f :"++~funcKey ++ "b : " ++ ~bankKey ).postln;
try{
if ((~modWheelUgens.matchAt(~bankKey)==nil) ,  {
		("bank key does not exist create key in the dictionary").postln;
			~modWheelUgens.add(~bankKey->Dictionary.new);
	// store in this dictionary second key and its associations
		("ccd::"++~ccData.matchAt(~bankKey).matchAt(~funcKey)).postln;
	~modWheelUgens.matchAt(~bankKey).add(~funcKey -> ~ccData.matchAt(~bankKey).matchAt(~funcKey));

		},
	{
		("bank key does  exist , check if function key matches").postln;
	// bank key does exist
	if((~modWheelUgens.matchAt(~bankKey).matchAt(~funcKey)==nil),{
		// function key does not exist, so store and create
		~modWheelUgens.matchAt(~bankKey).add(~funcKey -> ~ccData.matchAt(~bankKey).matchAt(~funcKey));
	},{
		// item exist, so it should be removed
		~modWheelUgens.matchAt(~bankKey).removeAt(~funcKey);
		// check the number of items in the dictionary, if nil remove it entirely
		if((~modWheelUgens.matchAt(~bankKey).size == nil) , {
				~modWheelUgens.removeAt(~bankKey);})

	});
	"item does exist".postln ;

	}
	);

};
	("mwu"+~modWheelUgens).postln ;
};

~modWheelUpdate = {
	| val | // this is a value between 0 and 1
	val.postln ;
	// iterate over dictionary
	//~dataString ="";

	try{
			w = 1;

		~modWheelUgens.do {
		|item , i |
			b = ~modWheelUgens.findKeyForValue(item);
		item.do {
			| jtem , j |
			f  = item.findKeyForValue(jtem);
			v = val.linlin(0,127 , jtem.at(\min) , jtem.at(\max));
			jtem.at(\ugen).set(jtem.at(\propertyName) , v);

				~temp = ( "" ++  b ++  "/" ++ (f-~ccNumOffset) ++ "/" ++ (round(val.linlin(0,127 ,0 , 1)*100)/100) ++ "/");
			//	~temp =  ("d" ++ f ++ "/" ++ b) ;
			//	~temp.postln;
				~dataString= ~dataString ++ ~temp ;

		//	(jtem).postln;

		}

	};

	};

// here we send the information out
	//	("---->"++~dataString).postln ;
		n.sendMsg("/ccm", ~dataString.asString);
        ~dataString ="";


};

/*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*   Defining the midibuffer - player function
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*/

~playSequence = {
	arg midiBuffer ;

// lets play the sequence

var dur , length , trigOff , midi , t_on , t_off ;
// define the trigOn and trigOff functions

d = midiBuffer.current.durs  ;
l = midiBuffer.current.lengths ;
m = midiBuffer.current.freqs ;


dur 	= 	Routine({loop({d.do({ arg dur ; dur.yield  })})});
length 	= 	Routine({loop({l.do({ arg len ; len.yield   })})});
midi 	= 	Routine({loop({m.do({ arg midi ; midi.yield  })})});

~r = Task({
	var delta ;
	while {
		delta = dur.next ;
		delta.notNil ;
	}{
		var deltaTrigOff , midiNote ;
		deltaTrigOff =length.next ;
		midiNote = midi.next ;
		~trigOn.value(midiNote);
		//("trig on :"+delta + " note : " + midiNote).postln ; // equivalent of trig on
		//TempoClock.default.sched(deltaTrigOff , { ("trig off :" + deltaTrigOff).postln;});
		TempoClock.default.sched(deltaTrigOff , ~trigOff );
		delta.yield ;
	} ;
}).play ;


};


/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
*
*  CONTROLLER CODE
*
*  this code maps the incoming cc-messages to the right functions
*  needs cleanup
*
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

MIDIdef.cc(\cc , {
	| key , func , ccNum , chan |
	("key"++key++"func"++func++"ccNum"++ccNum).postln ;

	case
	{ func == 1}{ ~modWheelUpdate.value(key) ; } // move the bank one up
	{ func == 116}{ ~bankKey= 0 ;} // move the bank one up
	{ func == 117}{ ~bankKey =1;} // move the bank one down
	{ func == 118}{ ~bankKey =2;} // move the bank one down
	{ func == 115}{ ~bankKey =3;} // move the bank one down
	{ func == 36}{
		case
		{key == 0}
		{
		try{
			~r.stop;
			};
	~p.initRecord;
	}
		{key == 1}{
			~p.stopRecord;
			~playSequence.value(~p);

		}
	}
	{ func == 37}{
		r.stop ;
	}
	{ func == 38}{
		r.play ;
	}
	// move the bank one down
	{ (func >= 20)&&(func<28) }{~knobButton.value(key , func , ccNum , chan);} //turn the knobs
	{ (func >= 48)&&(func<56) }{
		("firstrow").postln;
		//if((key==0) , {"het is nul".postln ; },{("het us iets anders:"++key).postln ;  });
		//~modWheelUpdate.value(key , func , ccNum , chan);
		if ((key==0),{~modWheelDictionaryUpdate.value(key , func-28 , ccNum , chan); }); // only when key value = 0
	} //turn the knobs
	{ (func >= 64)&&(func<71) }{("secondrow").postln;} //turn the knobs
	{ (func >= 80)&&(func<87) }{("thirdrow").postln;} //turn the knobs

	/* end cc medssage system here */

}
);


)


~synthDictionary