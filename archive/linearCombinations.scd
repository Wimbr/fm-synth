(

/*

excercise 1
-----------

this is a piece of code to illustrate the concept of a linear
combination in the form of

(where c1 .. cn are known constants)

s1 = c1 ;
s2 = s1 + c2 ;
s3 = s1 + s2 + c3 ;
...
si = s1 + ... + si + ci ;

s1 = 2
s2 = s1 + 5 = 7  ;
s3 = s1 + s2 + 8 = 9 + 8  = 17


*/


var source = Array.new(3);
var destination = Array.new(3);
source = [0 , 0 , 1];
destination = [2 , 5 , 8 ];

source.do{
	arg i , index ;
	n = 0 ;
	index.do {
		arg j , jindex ;
		n = n + source.at(jindex);
	};
	source.put(index , destination.at(index) + n) ;
};

)



(

/*

excercise 2
-----------

this is a piece of code to illustrate the concept of a linear
combination in the form of

(where c1 .. cn are known constants)

s1 =                    c1 ;
s2 = 1 * s1           + c2 ;
s3 = 0 * s1 +  1 * s2 + c3 ;
...
si = b1 * s1 + ... + bi*si + ci ;

s1 = 2
s2 = s1 + 5 = 7  ;
s3 = s1 + s2 + 8 = 9 + 8  = 17


*/

var c1 = 10 ;
var c2 = 100 ;
var c3 = 1000 ;

var source = Array2D.fromArray(3,3,
/*         s1, s2  c     */
	    [  0,  0,  c1,
		   1,  0,  c2,
		   1,  0,  c3     ] );
var results = Array.new(3);
results = [nil , nil , nil];



source.rowsDo{
	arg i , index ;
	n = 0 ;
	index.do {

		arg jindex ;
			n = n + results.at(jindex) * source.rowAt(index).at(jindex) ;
	};
		results.put(index , source.rowAt(index).at(2) + n) ;
};


results
)




(

SynthDef.new(\test , {

	arg a_source = [
		[0,0,0,0,0,0],
		[1,0,0,0,0,0],
		[0,1,0,0,0,0],
		[0,1,1,0,0,0],
		[0,0,0,0,0,0],
		[0,1,0,1,0,0],
		[0,1,0,0,0,0],
		[0,0,0,1,1,1]] , a = 220 ;
	"a".postln;
	s = SinOsc.ar(a);
	Out.ar(1,s);

}).add;

)


(


SynthDef(\fm7, {
	arg  a_matrix = [
		0,0,0,0,0,0,
		1,0,0,0,0,0,
		0,1,0,0,0,0,
		0,1,1,0,0,0,
		0,0,0,0,0,0,
		0,1,0,0,0,0,   // feedback out
		0,1,0,0,0,0,   // feedback In
		0,0,0,1,1,0,
		0,1,0,0,0,0
	],  //output
     pitch_bus
	, signalOutputBus
	, amp_bus
	, factor_bus
	, envelopes_inputBus
	, feedback_bus;



	var source = Array2D.fromArray(9,6,[0,0,0,0,0,0,
		1,0,0,0,0,0,
		0,1,0,0,0,0,
		0,1,1,0,0,0,
		0,0,0,0,0,0,
		0,1,0,0,0,0,   // feedback out
		0,1,0,0,0,0,   // feedback In
		0,0,0,1,1,0,
		0,1,0,0,0,0]);
//	source.postln;

	var results = Array.newClear(6);
	var feedbackOutRow = 6 ;
	var feedbackInRow = 7 ;
	var outputRow = 8 ;
	var outputSignal = 0  ;
	var pitch = In.kr(pitch_bus , 1 );
	var sin1 , sin2 , sin3 , sin4 , sin5 , sin6 , signal , localOut ;
	var feedback  = In.kr(feedback_bus , 1 );
	var envelopes = In.ar(envelopes_inputBus,6);
	var amp1,amp2,amp3,amp4,amp5,amp6 ;
	var factor1 , factor2, factor3 , factor4 , factor5 , factor6 ;
	var env1,env2,env3,env4,env5,env6;
	var envs = Array.newClear(6);
	var amps = Array.newClear(6);
	var factors = Array.newClear(6);
	var temp;
	#env1,env2,env3,env4,env5,env6 = In.ar(envelopes_inputBus,6);

	#amp1,amp2,amp3,amp4,amp5,amp6 = In.kr(amp_bus, 6) ;
	#factor1 , factor2, factor3 , factor4 , factor5 , factor6 =  In.kr(factor_bus ,6 ) ;

	envs = Array.newFrom(env1,env2,env3,env4,env5,env6) ;
	amps = Array.newFrom(amp1,amp2,amp3,amp4,amp5,amp6) ;
	factors = Array.newFrom(factor1,factor2,factor3,factor4,factor5,factor6);
	results = [0,0,0,0,0,0];



	// algo 2 ---------------------------------------------------------------------------------------------

	/*
	parametric algoritm , pseudo code

	*/

	//source is the matrix, it's this codes argument

	source.postln;

	source.cols.do {
	arg i , index ;
		temp = source.rowAt(feedbackInRow).at(index) * feedback*LocalIn.ar(1) ;
		index.do
		{
			arg j , jindex ;
			temp = temp + (results.at(jindex) * source.rowAt(index).at(jindex));
		};

	results.put( index , amps.at(index) * envs.at(index)*SinOsc.ar(factors.at(index) * pitch * (1 + temp ))) ;
	};



	localOut = results * source.rowAt(feedbackOutRow);
	outputSignal =  results * source.rowAt(outputRow);

	LocalOut.ar(SinOsc.ar(440));
	Out.ar(signalOutputBus , outputSignal);


}).add ;


)



QuarksGui.new
/*

this is an algorithm example.

*/
(
var algo = Array2D.fromArray(6,8,
	[
		0,0,0,0,0,0,0,0,
		1,0,0,0,0,0,0,1,
		0,0,0,0,0,0,0,0,
		0,1,1,0,0,0,0,0,
		0,0,0,0,0,0,0,0,
		0,0,0,0,0,1,0,0
	]);


// linear combinations of arrays work.

a = [0,1,1,0];

b = [10,0,4,1];

a*b

)

(

SynthDef(\fm7_properties_new , {
	| amp1     , amp2     , amp3     , amp4     , amp5     , amp6     ,
	factor1  , factor2  , factor3  , factor4  , factor5  , factor6  ,
	attack1  , attack2  , attack3  , attack4  , attack5  , attack6  ,
	release1 , release2 , release3 , release4 , release5 , release6 ,
	feedback ,
	amp_outputBus     ,
	factor_outputBus  ,
	attack_outputBus  ,
	release_outputBus ,
	feedback_outputBus , gate_bus , pitch_bus  , out_bus  |
	// feedback
	Out.kr(feedback_outputBus , feedback) ;
	// influence, amplitudes
	Out.kr(amp_outputBus , [amp1, amp2,amp3,amp4,amp5,amp6]) ;
	// factors
	Out.kr(factor_outputBus ,[factor1, factor2, factor3, factor4, factor5, factor6]) ;
	// envelope properties such as attack
	Out.kr(attack_outputBus , [attack1 ,attack2 ,attack3 ,attack4 ,attack5 ,attack6]) ;
	// .. and releasetimes
	Out.kr(release_outputBus , [release1 , release2,release3,release4,release5,release6]) ;

}).add ;


)


(

p = Array2D.fromArray(2,2,[0,1,1,0]);
p.rows


)