// FMSYNTH GUI GRID for SuperCollider FMSynth
//
//
//
//
//
//
// osc message classes
import oscP5.*;
import netP5.*;


// define osc variable
OscP5 oscP5;
// osc variables
NetAddress myRemoteLocation;
// this is the 2dimensional array containing the property names
String [][]properties;  
// general gui properties
int tileSize ; //size in pixels of each view
int numRows = 8 ; 
int numColumns = 8 ; 
// this is a 2 dimensional array containing the knobViews
KnobView [][] knobs = new KnobView[numRows][numColumns];
// these are variables containing pixeldata used by some drawingmodes of knobview / needs cleanup
PGraphics p, moon ;
// global GUI color set 
<<<<<<< HEAD
final color COLOR_BACKGROUND  = #000000 ;  
final color COLOR_RED         = #FE4660 ; 
final color COLOR_CYAN        = #00FEDA ; 
final color COLOR_YELLOW      = #fbba3b; 
final color COLOR_MAGENTA     = #ff5afe ;  
final color COLOR_PURPLE      = #534466 ; 
final color COLOR_DARK_GREEN  = #1B8387; 
// 
// frame rate vars, to be 
float curviness = .55 ; 
=======
final color COLOR_BACKGROUND = #000000 ;  
final color COLOR_RED = #FE4660 ; 
final color COLOR_CYAN = #00FEDA ; 
final color COLOR_YELLOW = #fbba3b; 
final color COLOR_MAGENTA = #ff5afe ;  
final color COLOR_PURPLE = #534466 ; 
// frame rate vars, to be 
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
float fr,pr_fr ; 

void setup(){
  frameRate(60);
  pr_fr = millis();
  size(800,800,P2D);
<<<<<<< HEAD
background(0); 
=======
 
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  // populate the propertieslist with unassigned strings, we haven't imported the names using the osc yet
   properties = new String[numRows][numColumns];
      // define array
   for (int j = 0 ; j < numRows ; j++)
   { 
          for (int i = 0 ; i < numColumns ; i++ )
          {
          properties[i][j] = "(unassigned)";
          }
          
   }
  

  // text properties

  tileSize = width/numRows; 
  textSize((int) width/80);
  textAlign(CENTER);

// iniitialization of image matrices
 p    = createGraphics(tileSize,tileSize);
 moon = createGraphics(tileSize,tileSize);


  // initiate the knpbview objects
  for(int i = 0 ; i < numRows ; i++){
    for(int j = 0 ; j < numColumns ; j++){
      KnobView knob = new KnobView(0);
      knob.origin = new PVector(j*width/numRows, i*height/numColumns);
<<<<<<< HEAD
     if (i<3) knob.mode = int(random(1,8));
=======
     if (i<3) knob.mode = int(random(1,6));
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
      knobs[i][j] = knob;
    }
}


// this is hardcoded assignment of the knobs, we ll do this after all knobview designs are implemented
// knobs[0][0].mode = KnobView.MODE_CASTLE ; 
// knobs[0][1].mode = KnobView.MODE_HOUR_GLASS;
// knobs[0][2].mode = KnobView.MODE_BOTTOM;
// knobs[0][3].mode = KnobView.MODE_MOON;
 /* start oscP5, listening for incoming messages at port 12000 */

    // start the OSC communication object
<<<<<<< HEAD
    oscP5 = new OscP5(this,7771);
    myRemoteLocation = new NetAddress("127.0.0.1",7770);
    // plug two responders, one for initalization (receiving the properties)
    // and one for receiving propertyvalue changes
    oscP5.plug(this, "ccInit", "/init");
    oscP5.plug(this, "knobResponder", "/cc");
    oscP5.plug(this, "modWheelResponder", "/ccm");
}

// this is the osc5 plug , when a property value is changed in supercollider
void knobResponder(int bankID , int funcID , float value){
 println("\ngot a message with address pattern cc");
=======
    oscP5 = new OscP5(this,12000);
    myRemoteLocation = new NetAddress("127.0.0.1",57120);
    // plug two responders, one for initalization (receiving the properties)
    // and one for receiving propertyvalue changes
    oscP5.plug(this, "ccInit", "/init");
    oscP5.plug(this, "ccResponder", "/cc");

}

// this is the osc5 plug , when a property value is changed in supercollider
void ccResponder(int bankID , int funcID , float value){
 //println("\ngot a message with address pattern cc");
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
 knobs[(int)bankID][(int)funcID].value = (float)value;
 knobs[(int)bankID][(int)funcID].updated =true;
 
}

// this is the osc5 plug CC init responder function when the SC propertylist is received via osc,
// it divides the resulting string and populates the properties array with the received string
// OSC init message format is  4/5/propertyName/0/1/propertyname/0/2 ... and so on, but not necessary in order
void ccInit(String s){
<<<<<<< HEAD
   println("\ngot a message with address pattern init");
=======
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  String[] parts = s.split("/");
  for(int i = 0 ; i < parts.length ; i+=3)
  {
        //                      x                  y          propertyname
<<<<<<< HEAD
       properties[parseInt(parts[i])][parseInt(parts[i+1])] = parts[i+2];}
=======
       properties[parseInt(parts[i])][parseInt(parts[i+1])] = parts[i+2];
  }
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
   // populate the labels
  for(int i = 0 ; i<numColumns ; i++){
    for (int j = 0 ; j < numRows ; j ++ ){
      knobs[i][j].label = properties[i][j]; 
    }
  }
}
<<<<<<< HEAD

// ***************************************************************************************************************
void modWheelResponder(String s){
// println("mwheelmessage: " + s);
  String[] parts = s.split("/");
 for(int i = 0 ; i < parts.length ; i+=3)
  {
        //                      bank              func        propertyname
      knobs[parseInt(parts[i])][parseInt(parts[i+1])].value = parseFloat(parts[i+2]);
      knobs[parseInt(parts[i])][parseInt(parts[i+1])].updated = true;

  }
   // populate the labels*/
 }
=======
// ***************************************************************************************************************
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
void draw(){
//  background(0); // we dont refresh the background because we only update view tiles that need to be updated
  
  for(int i=0 ; i< numRows  ; i++){
    for (int j=0 ; j<numColumns ; j++){
      KnobView k = knobs[i][j];
      if (k.updated == true){ 
        k.draw();
      }
    }
  }    
  

// crappy way of showing the frameRate in the application, needs review not urgent
  fill(#000000);
  rect(0,0,40,16);
  fr = millis() - pr_fr;
  textSize(10);
  fill(#00ff00);
<<<<<<< HEAD
  text(int(frameRate), 10 , 10 );
=======
  text(fr, 10 , 10 );
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  pr_fr = fr ;

};

interface Drawable {

  void draw();

}


class KnobView {
  // view properties
  final static int MODE_DEBUG = 0 ; 
  final static int MODE_CASTLE = 1 ; 
  final static int MODE_HOUR_GLASS = 2 ; 
  final static int MODE_BOTTOM = 3 ; 
  final static int MODE_MOON = 4 ; 
  final static int MODE_DIAMOND = 5 ;
<<<<<<< HEAD
  final static int MODE_THREE_LINES = 6 ;
  final static int MODE_HALF_WHEEL = 7 ;
=======
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  // flag to check if this view needs to be redrawn or not
  boolean updated = false ;
  // default propertyname
  String label = "(unassigned)";
  // the value is a publuc variable that will be between 0 and 1
  float value ; 
  // this is a public value that defines where the knobview will be drawn on the canvas
  PVector origin = new PVector(0,0);
  // this is a public variable holding the drawMode of the knob 
  int mode = MODE_DEBUG ;  

  KnobView(float initValue){
  value = initValue ; 
  //print(tileSize);
<<<<<<< HEAD


=======
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  }
  
  void draw(){
    // background
//  println("test"); 
<<<<<<< HEAD

=======
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  fill(0);
  rect(origin.x,origin.y,tileSize, tileSize);
   switch(mode){
    case MODE_DEBUG:
        float knobSize = 0.6 ;
        float labelHeight = 0.2 ;
        float labelWidth = 0.90 ;
        stroke(0);
        strokeWeight(.5);    
        fill(30);
        rect(origin.x,origin.y,tileSize,tileSize);
        // knob layout
        pushMatrix();
        translate(origin.x + tileSize/2 ,origin.y + tileSize*(1+labelHeight)/2);
        noStroke();
        pushMatrix();
        noStroke();
        fill(0);
        ellipse(0,0,tileSize*knobSize,tileSize*knobSize);
        strokeWeight(3);
        stroke(0,128,255);
       
        //line(0,0,100,100);
       popMatrix(); 
        rotate(map(value,0,1,0,2*PI));
        noFill();
        line(0,0,0,tileSize*knobSize/2);
       
        popMatrix();
        // label layout
        noStroke();
        fill(0);
        rect((1-labelWidth)/2*tileSize + origin.x,origin.y+6,tileSize*labelWidth,tileSize*labelHeight);
        if (label == "(unassigned)") 
        {fill(40);}else{  fill(255);};
      
        text(label, (1-labelWidth)/2*tileSize + origin.x , 6+ origin.y,tileSize*labelWidth,tileSize*labelHeight );
        /* add the label*/
        break;
    case MODE_CASTLE:
        noStroke();
        fill(COLOR_RED);
        float rectWidth = tileSize/5 ; 
        pushMatrix();
        translate(origin.x,origin.y);
        for (int i = 0 ; i < 3 ; i++){
          rect( i*2*tileSize/5,0,tileSize/5,value*tileSize );
        }
        fill(COLOR_CYAN);
        for (int i = 0 ; i < 2 ; i++){
        rect( (1+(i*2))*tileSize/5,(1-value)*tileSize,tileSize/5,value*tileSize );
        }


       popMatrix();
        break;
// upper recangles
     case MODE_HOUR_GLASS:
        noStroke();
        fill(COLOR_YELLOW);
        float xa = map(value,0,1,0,tileSize/2);
        pushMatrix();
        translate(origin.x,origin.y);
        beginShape();
          vertex(xa,0);
          vertex(tileSize-xa,0);
          vertex(xa,tileSize);
          vertex(tileSize-xa,tileSize);
        endShape(CLOSE);
       
        popMatrix();

        break;
     case MODE_BOTTOM:
        pushMatrix();
        translate(origin.x,origin.y);
        // draw ellipse
        float radiusCenterLeft = map(value,0,1,-50,-50-tileSize/2);
        float radiusCenterRight = map(value,0,1,tileSize+50,tileSize + tileSize/2+50);
      //:w
     p.beginDraw(); 
        p.fill(COLOR_BACKGROUND);
        p.noStroke();
        p.rect(0,0,tileSize,tileSize);
        p.ellipseMode(CENTER);
        p.fill(COLOR_RED);
        p.ellipse(radiusCenterLeft,tileSize/2,2*tileSize,2*tileSize);
        p.fill(COLOR_CYAN);
        p.ellipse(radiusCenterLeft,tileSize/2,1.4*tileSize,1.4*tileSize);
        p.fill(COLOR_RED);
        p.ellipse(radiusCenterRight,tileSize/2,2*tileSize,2*tileSize);
        p.fill(COLOR_MAGENTA);
        p.ellipse(radiusCenterRight,tileSize/2,1.4*tileSize,1.4*tileSize);
        p.endDraw();
        removeCache(p);
        image(p,0 , 0);
        popMatrix();       
        break;
// upper recangles
     case MODE_DIAMOND : 
        pushMatrix();
        translate(origin.x , origin.y);
        fill(COLOR_PURPLE);
        rect(0,0,tileSize,tileSize);
        fill(COLOR_RED);
        beginShape();
        vertex(tileSize/2,0);
        vertex(map(value,0,1,tileSize/2 , tileSize), tileSize/2);
        vertex(tileSize/2, tileSize) ; 
        endShape(CLOSE);
        fill(COLOR_YELLOW);
        beginShape();
        vertex(tileSize/2,0);
        vertex(tileSize/2, tileSize);
        vertex(map(value,0,1,tileSize/2 , 0 ), tileSize/2) ; 
        endShape(CLOSE);
        popMatrix();
        break ; 
     case MODE_MOON : 
        float radiusCutout =0.385*tileSize;
        // background
        pushMatrix();
        translate(origin.x + tileSize/2 , origin.y + tileSize/2);
        fill(COLOR_MAGENTA);
        rect(-tileSize/2,-tileSize/2,tileSize , tileSize) ; 
         // create crescent
        moon.beginDraw();
        moon.fill(COLOR_BACKGROUND);
        moon.rect(0,0,tileSize,tileSize);
        moon.fill(COLOR_CYAN);
        moon.ellipse(
             map(value ,0 , 1 , 0 , tileSize ),
          tileSize/2 ,2 *  radiusCutout ,2*  radiusCutout );
        moon.endDraw(); 
        removeCache(moon);
        image(moon,-tileSize/2,-tileSize/2); 
        
        
        popMatrix();


        // mask emulation
        pushMatrix();
        // move to middle of tile to draw the cutout shape
        translate(origin.x + tileSize/2, origin.y+tileSize/2);
       // float curviness = map(mouseX , 0 , width , 0 ,1) ; 
<<<<<<< HEAD
        PShape m ;
        m = createShape();
=======
       float curviness = .55 ; 
        PShape m = createShape();
>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
        m.beginShape();
        m.fill(COLOR_RED);
        m.vertex(0,radiusCutout);
        m.vertex(0,tileSize/2);
        m.vertex(tileSize/2 , tileSize/2);
        m.vertex(tileSize/2 , 0 );
        m.vertex(radiusCutout,0);
        m.bezierVertex( radiusCutout,curviness * radiusCutout,curviness * radiusCutout ,radiusCutout,0 , radiusCutout );
        m.endShape(CLOSE);
        
        shape(m);
        rotate(PI/2);
        shape(m);
        rotate(PI/2);
        shape(m);
        rotate(PI/2);
        shape(m);

        popMatrix();
        break;
<<<<<<< HEAD
      case MODE_THREE_LINES : 
        float smallRectHeight = .625 ; 
        float smallRectGutter = .20 ; 
        float smallRectWidth = .0667 ; 
        pushMatrix();
        translate(origin.x,origin.y + tileSize/2);
        fill(COLOR_PURPLE);
        rect(0,-tileSize/2,tileSize,tileSize);
        fill(COLOR_BACKGROUND);
        rect(smallRectGutter*tileSize ,( tileSize*(1 - smallRectHeight)/2)-tileSize/2 ,
            tileSize*smallRectWidth ,value* tileSize*smallRectHeight  );
        fill(COLOR_CYAN);
        rect((2*smallRectGutter + smallRectWidth)*tileSize , tileSize*(1 - smallRectHeight)/2-tileSize/2 , 
            tileSize*smallRectWidth ,value *  tileSize*smallRectHeight );
        fill(COLOR_YELLOW);
        rect((3*smallRectGutter + 2*smallRectWidth)*tileSize , tileSize*(1 - smallRectHeight)/2-tileSize/2 , 
            tileSize*smallRectWidth ,value *  tileSize*smallRectHeight);
        
        popMatrix();

        break;
        case MODE_HALF_WHEEL : 
        pushMatrix();
        translate(origin.x+tileSize/2,origin.y+tileSize/2);
       fill(COLOR_DARK_GREEN);
        rect(-tileSize/2,-tileSize/2,tileSize,tileSize);
        rotate(PI*value);
             PShape m1;
        m1 = createShape();
        m1.beginShape();
        m1.fill(COLOR_CYAN);
        m1.vertex(0 , -tileSize/2) ;
        m1.vertex(0 , tileSize/2);
        m1.bezierVertex(-curviness * tileSize/2 , tileSize/2 , -tileSize/2 , curviness * tileSize/2 , -tileSize/2 ,0 );
        m1.bezierVertex(-tileSize/2 ,-curviness*tileSize/2 , -curviness*tileSize/2 , -tileSize/2 , 0 , -tileSize/2 );
        m1.endShape(CLOSE);
        shape(m1);
        pushMatrix();
        rotate(PI);
        m1 = createShape();
        m1.beginShape();
        m1.fill(COLOR_BACKGROUND);
        m1.vertex(0 , -tileSize/2) ;
        m1.vertex(0 , tileSize/2);
        m1.bezierVertex(-curviness * tileSize/2 , tileSize/2 , -tileSize/2 , curviness * tileSize/2 , -tileSize/2 ,0 );
        m1.bezierVertex(-tileSize/2 ,-curviness*tileSize/2 , -curviness*tileSize/2 , -tileSize/2 , 0 , -tileSize/2 );
        m1.endShape(CLOSE);
        shape(m1);
                popMatrix();
        fill(COLOR_RED);
        ellipse(0,0,tileSize/3, tileSize/3);
        popMatrix();
        break;
=======


>>>>>>> d576066728f97bfb0233e72a2b482ceec1b228f6
  }
this.updated = false ; 
 }

};







