/*

Arperggiator Class inherits from Object class ( implicit in supercollider
<> are getter setter arguments

*/
Arpeggiator {
var <>active = false ,
	<>range = 1 ,
	<>noteList ,
	arpNoteList ,
	<>noteControl ,
	<>speed = 1 ,
	<clock ,
	polyphony ,
	synthindex = 0 ,
	<>triggerfunc ,
	<>arproutine ,
	arrayModulator ,
	<>mode = 0 ,  // the routine playing the arpeggiator
	speedList ;


	// instantiation, because class inherits from object

	*new1 { arg aclock , apolyphony, atriggerfunc ;
		^super.new.init(aclock , apolyphony , atriggerfunc)
	}


	// sort functions

	cascade_up {|arrayOfNotes| ^arrayOfNotes.sort({ arg a , b ; a < b })}
    cascade_down {|arrayOfNotes| ^arrayOfNotes.sort({ arg a , b ; a > b })}
	normal  {|arrayOfNotes| ^arrayOfNotes }
	reverse {|arrayOfNotes| ^arrayOfNotes.reverse }

	// the real init

	init {
		arg aclock , apolyphony , atriggerfunc ;
		polyphony = apolyphony ;
		arpNoteList  = List.new(polyphony);
		clock = aclock ;
		triggerfunc = atriggerfunc ;
		speed = 1;
		speedList = [0.125 , 0.25 , 0.5 , 0.75 , 1 , 2 , 4] ;
		this.initRoutine ; // call initRoutine function
		this.mode = 1 ;    // call mode function

	}

	registerNoteControl {
		arg aNoteControl ;

		noteControl = aNoteControl ;


	}

	clockDivide {
		arg divide ;
		this.speed = speedList.at(divide.floor.asInt);

	}

	initRoutine {

		arproutine = Routine({
			loop {
				range.floor.do {
					arg range_index ;
					// check if note
					switch(noteControl.mode ,
						Notecontrol.mode_polyphony ,
						{
							arpNoteList.do
							{
								|item|
								synthindex = synthindex + 1 ;
								synthindex = synthindex%(polyphony);
								triggerfunc.value(1 , 1 , item + (12  * range_index)  , synthindex ) ;
								(1/2 * speed).wait;
								//("called trigoff" ++ (~nn + item).midicps).postln;
								triggerfunc.value(1 , 0 , item  +  (12  * range_index) , synthindex ) ;
								(1/2 * speed).yield ;

							}
						} ,
						Notecontrol.mode_unison ,
						{

							arpNoteList.do
							{
								|item|
								//synthindex = synthindex + 1 ;
								//synthindex = synthindex%(polyphony);

								noteControl.polyphony.do
								{
									arg i ;
									var val ;
									val = i.linlin(0 , noteControl.polyphony , -1 * noteControl.detune , noteControl.detune);
									//val.postln;
									triggerfunc.value(1 , 1 , (item+val) + (12  * range_index)  , i ) ;

								};
								(1/2 * speed).wait;
								//("called trigoff" ++ (~nn + item).midicps).postln;
								noteControl.polyphony.do
								{
									arg i ;
									triggerfunc.value(1 , 0 , (item) + (12  * range_index)  , i ) ;

								};
								(1/2 * speed).yield ;






							}

					});
				}
		}});

	}




	/*active {
		arg activeFlag ;
		if (activeFlag == true){ activeState = true ;}{ activeState = false ; }
	}*/


	//external method method
	hello {
		"hello".postln ;
	}


	stop {
		if(arproutine.isPlaying){
			//"stopping arp".postln ;
			arproutine.stop;
			this.initRoutine ;
		}
	}


	setMode {
		arg aMode ;
		mode = aMode ;

		 arpNoteList = case
			{ this.mode.floor == 1 }  { this.cascade_up(noteList) }
			{ this.mode.floor == 2 }  { this.cascade_down(noteList) }
			{ this.mode.floor == 3 }  { this.normal(noteList) }
			{ this.mode.floor == 4 }  { this.reverse(noteList) } ;

	}


	update {
		arg aNoteList ;
			// update  the noteList
		//aNoteList.postln;
		noteList = aNoteList ;

		if (noteList.isEmpty){
			// the arp routine should stop immediately, otherwise we are in trouble

			this.stop ;
			//arproutine.stop;

			//arproutine.isPlaying = false ;
		}
		{
			// data is not nil, we can update the new noteList to our data variable

		 arpNoteList = case
			{ this.mode.floor == 1 }  { this.cascade_up(noteList) }
			{ this.mode.floor == 2 }  { this.cascade_down(noteList) }
			{ this.mode.floor == 3 }  { this.normal(noteList) }
			{ this.mode.floor == 4 }  { this.reverse(noteList) } ;

						// check if it  need to start
			if(arproutine.isPlaying.not){
				// arproutine is not playing we shold start it
				//"from Arp :: start arproutine...".postln ;
				//(arproutine).postln ;
				arproutine.play(clock , 0 ) ;
			}
			    // trigger the synths

		}{
			// the arproutine is not playing we should start it now

		}



		}

}
