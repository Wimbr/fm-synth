Notecontrol {
const <mode_polyphony = 0 ,
	<mode_unison = 1 ;
	var <>active = false ,
	<>noteQueue ,   /*

	the noteQueue is dependend of the polyphony, it is a set of maximum polyphony-elements that
	get added/removed on noteOn/noteOff Events , in later refactoring it should be treated as a
	separate object.

	*/
	<>notes , /*

	the notes are the notes currently being pressed by the keyboard ,
	its a fixed array of all possible notes (127) and notes are also given a timestamp

	*/


	<clock ,
	<>polyphony ,
	synthindex = 0 ,
	<>triggerfunc ,
	<>detune = 0 ,
	<>arpeggiator ,
	<>mode ,  					// the mode the notecontroller function right now this can be two values mode_polyphony / mode_unison
	<>detune = 0.5;   		    // the routine playing the arpeggiator


	// instantiation, because class inherits from object

	*new1 { arg aPolyphony ,  anArpeggiator , atriggerfunc , aMode ;
		^super.new.init(aPolyphony , anArpeggiator , atriggerfunc , aMode )
	}

	// the real init

	*getIndex {
		|itemToFind ,  array|
	     var index = -1 ;
		//"getIndex called...".postln;
		array.do
	     {
	          arg item ,i;
		      if (item == itemToFind) {
			   index = i ;};
         };
		//index.postln ;
	^index
	}




	init {
		arg aPolyphony  , anArpeggiator , atriggerfunc , aMode;
		triggerfunc = atriggerfunc ;
		polyphony = aPolyphony ;
		arpeggiator = anArpeggiator ;
		mode = aMode ;

		arpeggiator.registerNoteControl(this);
		// initiate the noteQueue
		noteQueue = Array.fill(polyphony) ;
        polyphony.do{
			|i|
			noteQueue.put(i , [nil , 0]); };

		// initiate the notes-array

		notes = List.new(127);

		// assign default note on function to clas variable

		// this is an evil line, it actually calls the function, and this will yield an error since no
		// args are given
	//	this.noteOn = this.polyNoteOn ;
		//this.noteOff = this.polyNoteOff ;

	}




	 noteOn {
		arg aNote ;
 	// find an item with nil
 	// there is an empty spot
 	// set note in this position and add timestamp
	// find the oldest spot


		var gateSelect , gate_trigger ;
	 	var synthIndex =0 ;
	 	var nonNilQueueNotes ;

		/*

		independed of the notequeue we should keep track of all the

		*/
		var timeStamp =  Date.getDate.rawSeconds  ;
		notes.add(aNote) ;
		notes.postln ;


		switch(mode ,
			mode_polyphony ,{

				// 1. find out the synthindex to play...

				// only the empty spots
				nonNilQueueNotes = noteQueue.reject({ arg item , i ; item.at(0).isNil.not });
				// 	// is this thing empty
				if (nonNilQueueNotes.isEmpty){
					// get the oldest spot of the noteQueue
					synthIndex = Notecontrol.getIndex(noteQueue.asList.maxItem({arg item ,i ; item.at(1)}) , noteQueue);
					gateSelect =  1  ; //triggergate
					gate_trigger = \t_gate ;
				}
				{
				// item contains a nil , pick one with nil
					synthIndex = Notecontrol.getIndex(nonNilQueueNotes.at(0) , noteQueue);
					gateSelect =  0  ; //defaultgate
					gate_trigger = \defaultGate ;
				} ;
				// 	 update note data , regardless of the arpgegiator -
				//we still need to keep track of whic notes are currently pressed
				//("noteQueue size" + noteQueue.size + "| synthIndex" + synthIndex).postln ;
				if((synthIndex == -1).not ) {
					//("putting " + aNote ).postln;
					noteQueue.put(synthIndex , [aNote ,timeStamp]) ;
					//("noteQueue"+ noteQueue).postln;
				};


				// 2. trigger the synth

				// this code  is for normal playing, triggering the synths in arp-mode is done by the arp-scheduler
				if(arpeggiator.active==false)
				{
					triggerfunc.value(1 , 1 , aNote , synthIndex);
					// little bits serializer
					//if(~serial == True) {~l.put((100 + (4 * (note-48))))} ;
				}
				{ // arpeggiator = active
					// arpeggiator.update( noteQueue.reject({ arg item , i ; item.at(0).isNil}).collect({arg item , i ; item.at(0)}));
					arpeggiator.update(notes);
				};

			} ,
			mode_unison , {

				// 1. now we just use all the indices to play at once, no need for note priority


				if(arpeggiator.active == false)
				{
					polyphony.do
					{
						arg index ;
						var val ;
						val = index.linlin(0 , polyphony , -1 * this.detune , this.detune);
						triggerfunc.value(1 , 1 , aNote+val , index);
					}
				}
				{
					arpeggiator.update(notes);
				}
		});
	}








	noteOff {
		arg aNote ;
		var synthIndex ;
		var timeStamp =  Date.getDate.rawSeconds  ;

		notes.remove(aNote);
			notes.postln ;

		switch(mode ,
			mode_polyphony , {
				synthIndex = Notecontrol.getIndex(aNote , noteQueue.collect({arg item , i ; item.at(0) })) ;
				// same as the note-on function, we still need to keep track which notes are played and which not
				if(synthIndex != -1) {
					// update noteQueue
					noteQueue.put(synthIndex , [nil , Date.getDate.rawSeconds]);
					if(arpeggiator.active==false){
						// trig the synth
						triggerfunc.value(0 , 0 , aNote , synthIndex);
					}{
						arpeggiator.update(notes);
					}
				};
				//("aNote : " + aNote  +"noteQueue - note off " + noteQueue).postln;
				//~noteQueue.postln;
			} ,
			mode_unison , {


				if(arpeggiator.active==false){

				polyphony.do {
					arg index ;
					triggerfunc.value(0 , 0 , aNote , index);
				};
				}{
					arpeggiator.update(notes);
				}

			}
	)

	}




	/*active {
		arg activeFlag ;
		if (activeFlag == true){ activeState = true ;}{ activeState = false ; }
	}*/


	//external method method




}
